'use strict'

/**
 * Creacion de la conexion a la BD
 */

//Modulos necesarios para conexion
var mysql = require('mysql');//NPM <- 

//Configuracion de conexion
let configuration = {

  host     : 'localhost',
  //host     : '192.168.43.215',
  user     : 'root',
  password : '',
  //password : 'root',
  database : 'heladeria'
  
}

//Creacion de la metodo de conexion
var link = mysql.createConnection( configuration );

//Exportando variable
module.exports = link;
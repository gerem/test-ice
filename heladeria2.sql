CREATE DATABASE  IF NOT EXISTS `heladeria` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `heladeria`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: heladeria
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `decoraciones`
--

DROP TABLE IF EXISTS `decoraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decoraciones` (
  `idDecoracion` int(11) NOT NULL AUTO_INCREMENT,
  `Decoracion` varchar(45) NOT NULL,
  PRIMARY KEY (`idDecoracion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decoraciones`
--

LOCK TABLES `decoraciones` WRITE;
/*!40000 ALTER TABLE `decoraciones` DISABLE KEYS */;
INSERT INTO `decoraciones` VALUES (1,'Cereal'),(2,'Chocolate'),(3,'Chispas de Chocolate'),(4,'Chispas de colores'),(5,'Granolado'),(6,'Mermelada');
/*!40000 ALTER TABLE `decoraciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `helados`
--

DROP TABLE IF EXISTS `helados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helados` (
  `idhelados` int(11) NOT NULL AUTO_INCREMENT,
  `idSabor` int(11) NOT NULL,
  `idTamanio` varchar(45) NOT NULL,
  `idDecoracion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idhelados`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `helados`
--

LOCK TABLES `helados` WRITE;
/*!40000 ALTER TABLE `helados` DISABLE KEYS */;
INSERT INTO `helados` VALUES (1,5,'3','3'),(2,1,'2','5'),(3,8,'2','6'),(4,8,'2','6'),(5,5,'2','5');
/*!40000 ALTER TABLE `helados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sabores`
--

DROP TABLE IF EXISTS `sabores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sabores` (
  `idSabor` int(11) NOT NULL AUTO_INCREMENT,
  `Sabor` varchar(45) NOT NULL,
  PRIMARY KEY (`idSabor`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sabores`
--

LOCK TABLES `sabores` WRITE;
/*!40000 ALTER TABLE `sabores` DISABLE KEYS */;
INSERT INTO `sabores` VALUES (1,'Almendrado'),(2,'Cereza'),(3,'Chocolate'),(4,'Coco'),(5,'Fresa'),(6,'Galleta Oreo'),(7,'Guanabana'),(8,'Limon'),(9,'Queso'),(10,'Napolitano'),(11,'Nuez'),(12,'Maracuya'),(13,'Menta'),(14,'Tequila'),(15,'Vainilla');
/*!40000 ALTER TABLE `sabores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tamanios`
--

DROP TABLE IF EXISTS `tamanios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tamanios` (
  `idTamanios` int(11) NOT NULL AUTO_INCREMENT,
  `Precio` int(11) NOT NULL,
  `Tamanio` varchar(45) NOT NULL,
  PRIMARY KEY (`idTamanios`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tamanios`
--

LOCK TABLES `tamanios` WRITE;
/*!40000 ALTER TABLE `tamanios` DISABLE KEYS */;
INSERT INTO `tamanios` VALUES (1,10,'pequeño'),(2,18,'mediano'),(3,23,'Grande');
/*!40000 ALTER TABLE `tamanios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-19 19:26:35

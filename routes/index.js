'use strict'

var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Inicio de server' });
});

router.get('/add', function(req, res, next) {
  res.render('add', { title: 'add elements' });
});

router.get('/update', function(req, res, next) {
  res.render('update', { title: 'update elements' });
});

router.get('/delete', function(req, res, next) {
  res.render('delete', { title: 'Eliminar' });
});

router.get('/name', function( req , res ){
	res
		.status(200)
		.json( { 
			"store" : { 
				"name" : "IceLabs"
			}
		}
		);
});

module.exports = router;

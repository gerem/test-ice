'use strict'; //Implementacion de elementos de ecma6

let express = require('express'); // Importamos la funcionalidad
let router = express.Router();    // Llamadas la funcionalidad de router
let connection = require('../models/Link');

//Creamos la conexion con el server
//connection.connect();

/*
 CRUD
 Creacion de helado id: - [OK]
 Lectura de Helados - [OK]
 Lectura de helado : id [OK]
 Actuaizar helado  : id [OK]
 Eliminar helado   : id []
 GET precios [Ok]
 GET sabores [OK]
*/


/**
 * Creacion de nuevo helado en base de datos.
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 */

router.post( '/' , function( req, res, next ){
	
	let data_post;//Contenido

	//Obtención de los datos
	if( req.body ){
		//Obtencion de los datos en POST
		data_post = req.body;	
	}
	else//Los datos de no fueron leidos
		res.status(400).json({error:'data can be readed'})

	//Conexion con base de datos
	//connection.connect();

	//Realizar peticion 
	let query = 'INSERT INTO `helados` '+
				'(`idhelados`, `idSabor`, `idTamanio`, `idDecoracion`)' + 
				'VALUES (?, ?, ?, ?);';

	//Hard coding
	let data = [
		null,
		data_post.idSabor,
		data_post.idTamanio,
		data_post.idDecoracion
	]
	
	connection.query( query , data , function (error, results, fields) {

		if(error){//Error en la conexion con la base de datos
			res
				.status(400)
				.json({ error : "data base error" })//Respuesta de id	
			throw error;

		}else{//Conexion exitosa
			//Verificar respuesta
			//REST
			res
				.status(201)
				.json({ id : results[0] })//Respuesta de id	
		}

	});
});

/**
 * Obtencion de todos los elementos 
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 * return : array of elements {}
 */
router.get('/', function(req, res, next) {

	//Inicio de conexion a la bd
	//connection.connect();

	//Sentencia
	let query = 'SELECT' +
				' sabores.Sabor,' +
			    ' tamanios.Tamanio,' +
			    ' tamanios.Precio,' +
				' decoraciones.Decoracion' +
				' FROM' +
					' helados' +
				' left join' +
					' sabores' +
				    ' on' +
						' helados.idSabor = sabores.idSabor' +
				' left join' + 
					' tamanios' +
				    ' on' +
						' helados.idTamanio = tamanios.idTamanios' +
				' left join' + 
					' decoraciones' +
				    ' on' +
						' helados.idDecoracion = decoraciones.idDecoracion;';

	//QUERY , [data replace] , callback
	connection.query( query , function (error, results, fields) {
	  
	  //En caso de error lanzar excepcion 
	 	if (error)
	  		throw error;

  		else{
			res
		  		.status(200) //Estado de peticion
		  		.json( { "icecreams" : results } );// Datos de la consulta
  		}

	});

	//Cerramos la conexion con la base de datos
	//connection.end();
});

/**
 * Obtencion de elemento puntual.
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 */
router.get('/:id', function(req, res, next) {

	let id = req.params.id;
	let query = 'SELECT * FROM `helados` WHERE id = ?';

	//Inicio de conexion a la bd
	//connection.connect();

	//Sentencia
	//QUERY , [data replace] , callback
	connection.query( query , id , function (error, results, fields) {
	  
	  //En caso de error lanzar excepcion 
	 	if (error)
	  		throw error;

  		else{
			res
				.status(200)
			  	.json( results[0] );

  		}

	});
});

/**
 * Update de elementos.
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 */
router.post( '/:id' , function( req, res, next ){
	
	let id_element = req.params.id;
	let data_post;//Contenido

	//Obtención de los datos
	if( req.body ){
		//Obtencion de los datos en POST
		data_post = req.body;	
	}
	else//Los datos de no fueron leidos
		res.status(400).json({error:'data can be readed'})

	//Conexion con base de datos
	//connection.connect();

	//Realizar peticion 
	let query = 'UPDATE `helados` ' + 
				'SET `id`=?, `sabor`=?, `tamanio`=?, `precio`=?, `decoracion`=? ' +
 				'WHERE id = ?;';

	//Hard coding
	let data = [
		id_element,
		data_post.sabor,
		data_post.tamanio,
		data_post.precio,
		data_post.decoracion,
		id_element
	]

	connection.query( query , data , function (error, results, fields) {

		if(error){//Error en la conexion con la base de datos
			res
				.status(400)
				.json({ error : "data base error" })//Respuesta de id	
			throw error;

		}else{//Conexion exitosa
			//Verificar respuesta
			res
			.status(200)
			.json( results )//Respuesta de id	
		}

	});
	
	//Cerrar conexion
	//connection.end();
});


/**
 * Eliminar de elementos.
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 */
router.post( '/delete/:id' , function( req, res, next ){
	
	let id_element = req.params.id;
	
	//Conexion con base de datos
	//connection.connect();

	//Realizar peticion 
	let query = 'DELETE FROM `helados` WHERE id = ?;';

	//Hard coding
	let data = [
		id_element
	]

	connection.query( query , data , function (error, results, fields) {

		if(error){//Error en la conexion con la base de datos
			res
				.status(400)
				.json({ error : "data base error" })//Respuesta de id	
			throw error;

		}else{//Conexion exitosa
			//Verificar respuesta
			res
			.status(200)
			.json( results )//Respuesta de id	
		}

	});
	
	//Cerrar conexion
	//connection.end();
});


module.exports = router;

'use strict'; //Implementacion de elementos de ecma6

let express = require('express'); // Importamos la funcionalidad
let router = express.Router();    // Llamadas la funcionalidad de router
let connection = require('../models/Link');

/**
 * Obtencion de todos los precios 
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 * return : array of elements {}
 */
router.get('/size', function(req, res, next) {

	//Inicio de conexion a la bd
	//connection.connect();

	//Sentencia
	//QUERY , [data replace] , callback
	connection.query('SELECT * FROM `tamanios`', function (error, results, fields) {
	  
	  //En caso de error lanzar excepcion 
	 	if (error)
	  		throw error;

  		else{
			res
		  		.status(200) //Estado de peticion
		  		.json( { "tamanio" : results } );// Datos de la consulta
  		}

	});

	//Cerramos la conexion con la base de datos
	//connection.end();
});


/**
 * Obtencion de todos los sabores 
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 * return : array of elements {}
 */
router.get('/flavor', function(req, res, next) {

	//Inicio de conexion a la bd
	//connection.connect();

	//Sentencia
	//QUERY , [data replace] , callback
	connection.query('SELECT * FROM `sabores`', function (error, results, fields) {
	  
	  //En caso de error lanzar excepcion 
	 	if (error)
	  		throw error;

  		else{
			res
		  		.status(200) //Estado de peticion
		  		.json( { "sabores" : results } );// Datos de la consulta
  		}

	});

});

/**
 * Obtencion de todos los sabores 
 * params : req : peticion
 * params : res : respuesta
 * params : next : callback promise
 * return : status : estado de respuesta
 * return : json : data
 * return : array of elements {}
 */
router.get('/decoration', function(req, res, next) {

	//Inicio de conexion a la bd
	//connection.connect();

	//Sentencia
	//QUERY , [data replace] , callback
	connection.query('SELECT * FROM `decoraciones`', function (error, results, fields) {
	  
	  //En caso de error lanzar excepcion 
	 	if (error)
	  		throw error;

  		else{
			res
		  		.status(200) //Estado de peticion
		  		.json( { "decoraciones" : results } );// Datos de la consulta
  		}

	});

});

module.exports = router;